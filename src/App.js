import React from "react";
import uuid from "uuid";

import { seedProfiles } from "./constants/seedProfiles";

import ProfileList from "./components/ProfileList";
import ProfileForm from "./components/ProfileForm";

import "./App.css";

class App extends React.Component {
  state = {
    title: "Upgrade UI",
    profiles: seedProfiles,
    editingProfile: null
  };

  createProfile = profile => {
    const newProfile = {
      ...profile,
      id: uuid()
    };

    const profiles = [...this.state.profiles, newProfile];

    this.setState({ profiles });
  };

  updateProfile = profile => {
    const profiles = this.state.profiles.map(prof => {
      return prof.id === profile.id ? profile : prof;
    });

    this.setState({ profiles });
  };

  setEditingProfile = profileId => {
    const editingProfile = this.state.profiles.find(
      prof => prof.id === profileId
    );

    this.setState({ editingProfile });
  };

  deleteProfile = id => {
    const profiles = this.state.profiles.filter(profile => profile.id !== id);

    this.setState({
      profiles
    });
  };

  render() {
    const { title, profiles, editingProfile } = this.state;

    return (
      <div>
        <h1>{title}</h1>
        <ProfileForm
          onSubmitProfile={this.createProfile}
          editingProfile={editingProfile}
          onEditProfile={this.updateProfile}
        />
        <ProfileList
          profiles={profiles}
          onClickDelete={this.deleteProfile}
          onClickEdit={this.setEditingProfile}
        />
      </div>
    );
  }
}

export default App;
