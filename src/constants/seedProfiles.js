export const seedProfiles = [
  {
    id: '1',
    name: "ignacio",
    age: 30,
    picture:
      "https://images.pexels.com/photos/736716/pexels-photo-736716.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
  },
  {
    id: '2',
    name: "pepe",
    age: 40,
    picture:
      "https://images.pexels.com/photos/736716/pexels-photo-736716.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
  }
];
