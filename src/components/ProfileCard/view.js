import React from "react";
import PropTypes from "prop-types";

import "./styles.scss";

class ProfileCard extends React.Component {
  static defaultPicture =
    "https://portal.staralliance.com/imagelibrary/aux-pictures/prototype-images/avatar-default.png/@@images/image.png";

  handleOnClickEdit = () => {
    const { onClickEdit, profile } = this.props;
    onClickEdit(profile.id);
  };

  handleClickDelete = () => {
    const { onClickDelete, profile } = this.props;
    onClickDelete(profile.id);
  };

  render() {
    const { profile, onClickDelete, onClickEdit } = this.props;
    const { name, age, picture } = profile;

    return (
      <div className="ProfileCard__container">
        <img
          src={picture || ProfileCard.defaultPicture}
          alt={`${name}'s avatar`}
          width="150"
        />
        <p>
          <b>Name:</b> {name}
        </p>
        <p>
          <b>Age:</b> {age}
        </p>
        <div>
          {onClickEdit ? (
            <button
              className="ProfileCard__button ProfileCard--edit"
              type="button"
              onClick={this.handleOnClickEdit}
            >
              Edit
            </button>
          ) : null}
          {onClickDelete ? (
            <button
              className="ProfileCard__button ProfileCard--danger"
              type="button"
              onClick={this.handleClickDelete}
            >
              Delete
            </button>
          ) : null}
        </div>
      </div>
    );
  }
}

ProfileCard.propTypes = {
  profile: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    age: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    picture: PropTypes.string
  }).isRequired,
  onClickEdit: PropTypes.func,
  onClickDelete: PropTypes.func
};

ProfileCard.defaulProps = {
  onClickEdit: null,
  onClickDelete: null
};

export default ProfileCard;
