import React from "react";
import PropTypes from "prop-types";

import { formFields, initialFormState } from "./constants";

import ProfileCard from "../ProfileCard";

import "./styles.scss";

class ProfileForm extends React.Component {
  state = initialFormState;

  static getDerivedStateFromProps(nextProps, prevState) {
    const { editingProfile } = nextProps;
    const { id } = prevState;

    if (editingProfile && editingProfile.id !== id) {
      return nextProps.editingProfile;
    }

    return prevState;
  }

  handleSubmit = e => {
    e.preventDefault();
    const { id } = this.state;

    if (id) {
      this.props.onEditProfile(this.state);
    } else {
      this.props.onSubmitProfile(this.state);
    }

    this.setState(initialFormState);
  };

  handleOnChangeName = e => {
    const { value } = e.target;

    this.setState({
      name: value.trim()
    });
  };

  handleOnChangeAge = e => {
    const { valueAsNumber } = e.target;

    this.setState({
      age: Number.isNaN(valueAsNumber) ? "" : valueAsNumber
    });
  };

  handleOnChangePicture = e => {
    const { value } = e.target;

    this.setState({
      picture: value.trim()
    });
  };

  validateProfile() {
    return !(this.state.name && this.state.age);
  }

  render() {
    const formFunctionMap = {
      name: this.handleOnChangeName,
      age: this.handleOnChangeAge,
      picture: this.handleOnChangePicture
    };

    return (
      <form className="ProfileForm__container" onSubmit={this.handleSubmit}>
        <fieldset>
          <legend>Create a new profile</legend>

          <div className="ProfileForm__content">
            <div className="ProfileForm__form-fields">
              {formFields.map(({ id, label, ...rest }) => (
                <label key={id} htmlFor={id}>
                  <span>{label}</span>
                  <input
                    id={id}
                    value={this.state[id]}
                    onChange={formFunctionMap[id]}
                    {...rest}
                  />
                </label>
              ))}
              {/* <span>Setting fields one by one example</span>
            <label htmlFor="name">
              <span>Name</span>
              <input
                id="name"
                type="text"
                value={name}
                onChange={this.handleOnChangeName}
              />
            </label>
            <label htmlFor="age">
              <span>Age</span>
              <input
                id="age"
                type="number"
                min={1}
                value={age}
                onChange={this.handleOnChangeAge}
              />
            </label>
            <label htmlFor="picture">
              <span>Picture</span>
              <input
                id="picture"
                type="text"
                value={picture}
                onChange={this.handleOnChangePicture}
              />
            </label> */}
              <button
                disabled={this.validateProfile()}
                className="ProfileForm--submit"
                type="submit"
              >
                Submit
              </button>
            </div>

            <ProfileCard profile={this.state} />
          </div>
        </fieldset>
      </form>
    );
  }
}

ProfileForm.propTypes = {
  editingProfile: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    age: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    picture: PropTypes.string
  }),
  onSubmitProfile: PropTypes.func.isRequired,
  onEditProfile: PropTypes.func.isRequired
};

ProfileForm.defaultProps = {
  editingProfile: null
};

export default ProfileForm;
